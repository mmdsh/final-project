
$(document).ready(function(){
	"use strict";

	var window_width 	 = $(window).width(),
	window_height 		 = window.innerHeight,
	header_height 		 = $(".default-header").height(),
	header_height_static = $(".site-header.static").outerHeight(),
	fitscreen 			 = window_height - header_height;


    var element = jQuery('#style-changer');

    if( element ) {
        $.ajax({
            url: element.attr('data-src'),
            type: 'get',
            dataType: 'text',
            success: function(data) {
                var newContent = jQuery('<div>', {
                    html: data
                });

                newContent.appendTo(element);
            }
        });
    }

	
	
	$(".fullscreen").css("height", window_height)
	$(".fitscreen").css("height", fitscreen);

    //-------- Active Sticky Js ----------//
     $(".sticky-header").sticky({topSpacing:0});

     $('select').niceSelect();

    // Add smooth scrolling to Menu links
     $(".nav-link, .dropdown-ct a, .banner-area a").on('click', function(event) {
            if (this.hash !== "") {
              event.preventDefault();
              var hash = this.hash;
              $('html, body').animate({
                scrollTop: $(hash).offset().top - (-10)
            }, 600, function(){
             
                window.location.hash = hash;
            });
        } 
    });

    //------- Color Changer Open --------//
    $(".changer-open").on('click', function(e) {
        e.preventDefault();
        $(".color-changer").toggleClass('toggle-changer');
    });

    //--------- Accordion Icon Change ---------//

    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".lnr-chevron-down").removeClass("lnr-chevron-down").addClass("lnr-chevron-up");

    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".lnr-chevron-up").removeClass("lnr-chevron-up").addClass("lnr-chevron-down");
    });
    $(document).ready(function(){
        $(".owl-carousel").owlCarousel();
      });
    //--------- Banner Content Carousel ---------//

    $('.active-banner-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        autoplay:true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        rtl:true,
    })

    //--------- Book Review Carousel ---------//

    $('.active-review-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        autoplay:true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
    })
    
    $('.active-review-carousel-solid').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        autoplay:true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
    })

    //--------- Book Inside Page Carousel ---------//

    $('.book-inside-carousel').owlCarousel({
        loop: true,
        nav: false,
        items: 3,
        margin: 20,
        autoHeight : true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        animateOut: 'fadeOut',
        responsive: {
              0:{
                items:1,
                nav:false,
                autoplay:true,
                autoplayTimeout:1500,
                autoplayHoverPause:true,
                margin: 0
            },
            768:{
                items:2,
                nav: true
            },
            1000:{
                items:3,
            }
        },
        rtl:true,
    })
    $('.book-inside-carousel-solid').owlCarousel({
        loop: true,
        nav: true,
        navText: ['<span class="lnr lnr-arrow-left"></span>', '<span class="lnr lnr-arrow-right"></span>'],
        items: 3,
        margin: 20,
        autoHeight : true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        animateOut: 'fadeOut',
        responsive: {
              0:{
                items:1,
                nav:false,
                autoplay:true,
                autoplayTimeout:1500,
                autoplayHoverPause:true,
                margin: 0
            },
            768:{
                items:2,
                nav: true
            },
            1000:{
                items:3,
            }
        }
    })
    $('.weekly-book-carousel').owlCarousel({
        loop: true,
        nav: true,
        navText: ['<span class="lnr lnr-arrow-left"></span>', '<span class="lnr lnr-arrow-right"></span>'],
        items: 1,
        margin: 20,
        autoHeight : true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        animateOut: 'fadeOut',
    })
    $('.active-more-books').owlCarousel({
        loop: true,
        nav: true,
        navText: ['<span class="lnr lnr-arrow-left"></span>', '<span class="lnr lnr-arrow-right"></span>'],
        items: 3,
        margin: 20,
        autoHeight : true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        animateOut: 'fadeOut',
        responsive: {
              0:{
                items:1,
                nav:false,
                autoplay:true,
                autoplayTimeout:1500,
                autoplayHoverPause:true,
                margin: 0
            },
            440:{
                items:2,
                nav: false
            },
            768:{
                items:3,
                nav: false
            },
            1000:{
                items:4,
            }
        }
    })
    $('.active-more-books-solid').owlCarousel({
        loop: true,
        nav: true,
        navText: ['<span class="lnr lnr-arrow-left"></span>', '<span class="lnr lnr-arrow-right"></span>'],
        items: 3,
        margin: 20,
        autoHeight : true,
    	autoplayTimeout:1500,
    	autoplayHoverPause:true,
    	animateOut: 'fadeOut',
    	responsive: {
    		  0:{
                items:1,
                nav:false,
                autoplay:true,
                autoplayTimeout:1500,
                autoplayHoverPause:true,
                margin: 0
            },
            440:{
                items:2,
                nav: false
            },
            768:{
                items:3,
                nav: false
            },
            1000:{
                items:4,
            }
    	}
    })

    $('.gallery').magnificPopup({
      type: 'image',
      gallery:{
        enabled:true,
         preload: [0,2], // read about this option in next Lazy-loading section

          navigateByImgClick: true,

          arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

          tPrev: 'Previous (Left arrow key)', // title for left button
          tNext: 'Next (Right arrow key)', // title for right button
          tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
        }
    });

    // -------   Mail Send ajax

    $(document).ready(function() {
        var form = $('#myForm'); // contact form
        var submit = $('.submit-btn'); // submit button
        var alert = $('.alert'); // alert div for show alert message

        // form submit event
        form.on('submit', function(e) {
            e.preventDefault(); // prevent default form submit

            $.ajax({
                url: 'mail.php', // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                data: form.serialize(), // serialize form data
                beforeSend: function() {
                    alert.fadeOut();
                    submit.html('Sending....'); // change submit button text
                },
                success: function(data) {
                    alert.html(data).fadeIn(); // fade in response data
                    form.trigger('reset'); // reset form
                    submit.html(''); // reset submit button text
                },
                error: function(e) {
                    console.log(e)
                }
            });
        });
    });


    //--------- Active Video Play -----------//
    $(".play-btn").magnificPopup({
        type: 'iframe',
        patterns: {
            youtube: {
                index: 'youtube.com',
                    id: 'v=',
                    src: '//www.youtube.com/embed/%id%?autoplay=1'
                }
        }
    });


    });
    $(document).ready(function() {
        $('#mc_embed_signup').find('form').ajaxChimp();
    });
    $(document).ready(function(){
        $(".owl-carousel").owlCarousel();
      });
      
      $('.special-carousel').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        rtl:true,
        navText : ['<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>','<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>'],
    });
	$('.second-special-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 2,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        rtl:true,
		responsive:{
            0:{
                items: 1
              },
            480:{
            items: 1
            },
            769:{
            items:2
            }
        },
    });
    /*----------Persian Numbers----------*/
    function walkNode(node) { 
        if (node.nodeType == 3) {
            // Do your replacement here
            node.data = node.data.replace(/\d/g,convert);
        }
    
        // Also replace text in child nodes
        for (var i = 0; i < node.childNodes.length; i++) {
            walkNode(node.childNodes[i]); 
        }
    }
    
    walkNode(document.getElementsByTagName('body')[0]);
    
    function convert(a){
        return ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'][a];
    }
    /*--------- Login ---------*/
    var username;
    var newEmail;
    var newUsername;
    var newPassword;
    function sign(){
        $("#pass-input").hide();
        $("#reset-pass").hide();
        $( "#message" ).empty();
    }
    $('#sign-up').click(sign());
    $('#next-mission').click(function(event){
        event.preventDefault();
        if($('#username').val().length != 0)
        {
        username = $('#username').val()
        console.log(username);
        $('#username-input').hide();
        $('#pass-input').show();
        }
    });
    $('.forgot').click(function(){
        $("#reset-pass").show();
        $('#pass-input').hide();
        $( "#message" ).empty();
    });
    $('#create-account').click(function(){
        $('#register-username-input').hide();
        $('#register-password-input').hide();
        $('#register-email-input').show();

    });
    $('#register-next-mission').click(function(event){
        event.preventDefault();
        if($('#email1').val().length != 0)
        {
            newEmail = $('#email1').val();
            console.log(newEmail);
            $('#register-email-input').hide();
            $('#register-password-input').hide();
            $('#register-username-input').show();
        }
    });
    $('#go-to-login').click(function(){
        $('#register-email-input').hide();
        $('#pass-input').hide();
        $("#reset-pass").hide();
        $('#username-input').show();
        $( "#message" ).empty();
    });
    $('#log-in').click(function(event){
        event.preventDefault();
        var pass = $('#pass').val();
        console.log(pass);
        $.ajax({
            url: 'http://api.milogy.com/authentication/login',
            headers: {
                'Content-Type' : 'application/json',
                'Application' : 'wKvWyMJV5ab85DMYKpOD'
            },
            type: 'POST',
            data: {username:username , password:pass},
            success:function(res){
                $( "#message" ).empty();
                $( "#message" ).append( "<p class='alert alert-success'>شما با موفقیت وارد شدید</p>" );
            },
            error:function(res){
                $( "#message" ).empty();
                $( "#message" ).append( "<a class='alert alert-danger' href='#' id='error'>نام کاربری یا رمز عبور صحیح نیست .برای تلاش دوباره اینجا کلیک کنید</a>" );
            },
        })
    });
    $('body').on('click','#error',function(){
        $("#pass-input").hide();
        $("#reset-pass").hide();
        $( "#message" ).empty();
        $("#username-input").show();
    });
    
    /*----------Reset Pass -----------*/

    $('#send-reset-email').click(function(event){
        event.preventDefault();
        if($('#reset-email').val.length != 0)
        {
            var resetEmail = $('#reset-email').val();
            $.ajax({
                url: 'http://api.milogy.com/users/reset',
                headers: {
                    'Content-Type' : 'application/json',
                    'Application' : 'wKvWyMJV5ab85DMYKpOD'
                },
                type: 'POST',
                data: {string: resetEmail},
                success:function(res){
                    $( "#message" ).empty();
                    $( "#message" ).append( "<p class='alert alert-success'>ایمیلی حاوی لینک بازیابی رمز به ایمیل شما ارسال شد</p>" );
                },
                error:function(res){
                    $( "#message" ).empty();
                    $( "#message" ).append( "<a class='alert alert-danger' href='#'>نام کاربری یا ایمیل صحیح نیست .</a>" );
                },
            })
        }
    });

    /*--------------- Register -----------------*/

    $('#register-second-mission').click(function(event){
        event.preventDefault();
        if($('#register-username').val().length != 0)
        {
            newUsername = $('#register-username').val();
            console.log(newUsername);
            $('#register-email-input').hide();
            $('#register-password-input').show();
            $('#register-username-input').hide();
        }
    });

    $('#register-last-mission').click(function(event){
        event.preventDefault();
        if($('#register-password').val().length != 0)
        {
            newPassword = $('#register-password').val();
            console.log(newPassword);
            $.ajax({
                url: 'http://api.milogy.com/users/register',
                headers: {
                    'Content-Type' : 'application/json',
                    'Application' : 'wKvWyMJV5ab85DMYKpOD'
                },
                type: 'POST',
                data: {email:newEmail , username:newUsername , password : newPassword},
                crossDomain: true,
                success:function(){
                    $( "#message2" ).empty();
                    $( "#message2" ).append( "<p class='alert alert-success'>ثبت نام شما با موفقیت انجام شد</p>" );
                },
                error:function(msg){
                    $( "#message2" ).empty();
                    console.log(msg);
                    $( "#message2" ).append( "<a class='alert alert-danger' href='#' id='reset-register'>ایمیل یا نام کاربری مورد نظر قبلا ثبت شده.لطفا مقادیر دیگری را انتخاب کنید.</a>" );
                }
            })
        }
    });
    
    $('body').on('click','#reset-register',function(){
        $("#register-password-input").hide();
        $( "#message2" ).empty();
        $("#register-email-input").show();
    });
